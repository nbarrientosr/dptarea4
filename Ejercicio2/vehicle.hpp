
#include "antena.hpp"

/////Clase padre Vehiculo

class Vehicle {
protected:
    AntennaImplementation* antennaImplementation; /// declaracion puntero

public:
    Vehicle(AntennaImplementation* impl) : antennaImplementation(impl) {}

    virtual void setup() = 0;
};
