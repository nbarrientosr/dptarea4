#include <iostream>
#include <string>
#include "vehicle.hpp"
#include "bike.hpp"
#include "car.hpp"
#include "antenaWan.hpp"
#include "antennaFiveG.hpp"

int main() {
    // Crear implementaciones de antenas
    AntennaImplementation* wanAntenna = new WANAntennaImpl();  /// Instanacia a la antena WAN
    AntennaImplementation* fiveGAntenna = new antennaFG(); /// Instanacia a la antena 5G
    // Configurar motocicleta con antena WAN
    
     Vehicle* moto = new Motorcycle(wanAntenna); // Instancia a motocicleta
     moto->setup();

    std::cout << std::endl;

    // Configurar carro sedan con antena 5G


    Vehicle* carro = new Car(fiveGAntenna); // Instancia a vehiculo sedan
    carro->setup();


    return 0;
}