

class Motorcycle : public Vehicle { // Clase motocicleta hereda de vehiculo
public:
    Motorcycle(AntennaImplementation* antennaImplementation) : Vehicle(antennaImplementation) {}

    void setup() override {/// Override de setup para moto
        std::cout << "Configurando motocicleta" << std::endl;
        antennaImplementation->setupAntenna();
    }
};