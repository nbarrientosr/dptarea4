

class Car : public Vehicle {
public:
    Car(AntennaImplementation* antennaImplementation) : Vehicle(antennaImplementation) {}

    void setup() override { /// Override de setup para auto sedan
        std::cout << "Configurando automóvil" << std::endl;
        antennaImplementation->setupAntenna();
    }
};
