

class WANAntennaImpl : public AntennaImplementation { // Clase antena Wan hereda de clase antena
public:
    void setupAntenna() override { /// Override implementa antena especifica
        std::cout << "Configurando antena WAN..." << std::endl;
    }
};