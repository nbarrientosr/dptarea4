public class AudioFacade
    {
        private AudioMixer audioMixer;
        private AudioConverter audioConverter;

        public AudioFacade(AudioMixer audioMixer, AudioConverter audioConverter)
        {
            this.audioMixer = audioMixer;
            this.audioConverter = audioConverter;
        }

        public string ExportAudioMix()
        {
            string mixedAudio = audioMixer.MixAudio();
            string convertedAudio = audioConverter.ConvertAudio(mixedAudio);
            return convertedAudio;
        }
    }