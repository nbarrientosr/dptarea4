public class LaunchPadBuilder
    {
        private LaunchPad launchPad;

        public LaunchPadBuilder()
        {
            launchPad = new LaunchPad();
        }

        public LaunchPadBuilder SetHorizontalButtonsCount(int count)
        {
            launchPad.HorizontalButtonsCount = count;
            return this;
        }

        public LaunchPadBuilder SetVerticalButtonsCount(int count)
        {
            launchPad.VerticalButtonsCount = count;
            return this;
        }

        public LaunchPadBuilder SetRGBLightsEnabled(bool enabled)
        {
            launchPad.RGBLightsEnabled = enabled;
            return this;
        }

        public LaunchPadBuilder SetUsbTypeCPortEnabled(bool enabled)
        {
            launchPad.UsbTypeCPortEnabled = enabled;
            return this;
        }

        public LaunchPadBuilder SetJackInputEnabled(bool enabled)
        {
            launchPad.JackInputEnabled = enabled;
            return this;
        }

        public LaunchPadBuilder SetMicroSDCardSlotEnabled(bool enabled)
        {
            launchPad.MicroSDCardSlotEnabled = enabled;
            return this;
        }

        public LaunchPadBuilder SetUSBStorageEnabled(bool enabled)
        {
            launchPad.USBStorageEnabled = enabled;
            return this;
        }

        public LaunchPad Build()
        {
            return launchPad;
        }
    }
