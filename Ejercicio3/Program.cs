﻿using System;

namespace LaunchPadSimulation
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion = MostrarMenu();
            LaunchPadBuilder launchPadBuilder = new LaunchPadBuilder();

            while (opcion != 0)
            {
                switch (opcion)
                {
                    case 1:
                        LaunchPad launchPad = CrearLaunchPadPersonalizado(launchPadBuilder);
                        Console.WriteLine("Se creó un nuevo LaunchPad:");
                        Console.WriteLine(launchPad.ToString());
                        break;
                    case 2:
                        LaunchPad copy = launchPadBuilder.Build().DeepCopy();
                        Console.WriteLine("Se creó una copia del LaunchPad:");
                        Console.WriteLine(copy.ToString());
                        break;
                    case 3:
                        VerificarSincronizacion();
                        break;
                    default:
                        Console.WriteLine("Opción inválida. Por favor, selecciona una opción válida del menú.");
                        break;
                }

                opcion = MostrarMenu();
            }

            Console.WriteLine("Programa finalizado. ¡Hasta luego!");
        }

        static int MostrarMenu()
        {
            Console.WriteLine("======= Menú =======");
            Console.WriteLine("1. Crear nuevo LaunchPad personalizado");
            Console.WriteLine("2. Crear copia de LaunchPad");
            Console.WriteLine("3. Verificar sincronización con el módulo Tester");
            Console.WriteLine("0. Salir");
            Console.WriteLine("====================");
            Console.Write("Selecciona una opción: ");

            int opcion;
            while (!int.TryParse(Console.ReadLine(), out opcion))
            {
                Console.Write("Opción inválida. Por favor, selecciona una opción válida: ");
            }

            Console.WriteLine();

            return opcion;
        }

        static void VerificarSincronizacion()
        {
            Console.WriteLine("Verificando sincronización con el módulo Tester...");
            // Lógica para verificar la sincronización con el módulo Tester
            Console.WriteLine("Sincronización exitosa.");
            Console.WriteLine();
        }

        static LaunchPad CrearLaunchPadPersonalizado(LaunchPadBuilder launchPadBuilder)
        {
            Console.WriteLine("======= Crear LaunchPad personalizado =======");
            Console.Write("Ingrese el número de botones horizontales: ");
            int horizontalButtonsCount = int.Parse(Console.ReadLine());

            Console.Write("Ingrese el número de botones verticales: ");
            int verticalButtonsCount = int.Parse(Console.ReadLine());

            Console.Write("¿Habilitar luces RGB? (true/false): ");
            bool rgbLightsEnabled = bool.Parse(Console.ReadLine());

            Console.Write("¿Habilitar puerto USB Type-C? (true/false): ");
            bool usbTypeCPortEnabled = bool.Parse(Console.ReadLine());

            Console.Write("¿Habilitar entrada de jack? (true/false): ");
            bool jackInputEnabled = bool.Parse(Console.ReadLine());

            Console.Write("¿Habilitar ranura para tarjeta MicroSD? (true/false): ");
            bool microSDCardSlotEnabled = bool.Parse(Console.ReadLine());

            Console.Write("¿Habilitar almacenamiento USB? (true/false): ");
            bool usbStorageEnabled = bool.Parse(Console.ReadLine());

            LaunchPad launchPad = launchPadBuilder
                .SetHorizontalButtonsCount(horizontalButtonsCount)
                .SetVerticalButtonsCount(verticalButtonsCount)
                .SetRGBLightsEnabled(rgbLightsEnabled)
                .SetUsbTypeCPortEnabled(usbTypeCPortEnabled)
                .SetJackInputEnabled(jackInputEnabled)
                .SetMicroSDCardSlotEnabled(microSDCardSlotEnabled)
                .SetUSBStorageEnabled(usbStorageEnabled)
                .Build();

            return launchPad;
        }
    }
}
