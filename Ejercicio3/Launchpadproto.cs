public class LaunchPadPrototype
    {
        private LaunchPad launchPad;

        public LaunchPadPrototype(LaunchPad launchPad)
        {
            this.launchPad = launchPad;
        }

        public LaunchPad Clone()
        {
            return new LaunchPad()
            {
                HorizontalButtonsCount = launchPad.HorizontalButtonsCount,
                VerticalButtonsCount = launchPad.VerticalButtonsCount,
                RGBLightsEnabled = launchPad.RGBLightsEnabled,
                UsbTypeCPortEnabled = launchPad.UsbTypeCPortEnabled,
                JackInputEnabled = launchPad.JackInputEnabled,
                MicroSDCardSlotEnabled = launchPad.MicroSDCardSlotEnabled,
                USBStorageEnabled = launchPad.USBStorageEnabled
            };
        }
    }