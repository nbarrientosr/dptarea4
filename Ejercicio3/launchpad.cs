 public class LaunchPad
    {
        public int HorizontalButtonsCount { get; set; }
        public int VerticalButtonsCount { get; set; }
        public bool RGBLightsEnabled { get; set; }
        public bool UsbTypeCPortEnabled { get; set; }
        public bool JackInputEnabled { get; set; }
        public bool MicroSDCardSlotEnabled { get; set; }
        public bool USBStorageEnabled { get; set; }

        public LaunchPad DeepCopy()
        {
            return (LaunchPad)this.MemberwiseClone();
        }

        public override string ToString()
        {
            return $"Horizontal Buttons: {HorizontalButtonsCount}\n" +
                   $"Vertical Buttons: {VerticalButtonsCount}\n" +
                   $"RGB Lights Enabled: {RGBLightsEnabled}\n" +
                   $"USB Type-C Port Enabled: {UsbTypeCPortEnabled}\n" +
                   $"Jack Input Enabled: {JackInputEnabled}\n" +
                   $"MicroSD Card Slot Enabled: {MicroSDCardSlotEnabled}\n" +
                   $"USB Storage Enabled: {USBStorageEnabled}";
        }
    }

