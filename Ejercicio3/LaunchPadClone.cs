  public static class DeepCopyExtensions
    {
        public static LaunchPad DeepCopy(this LaunchPad launchPad)
        {
            return new LaunchPad
            {
                HorizontalButtonsCount = launchPad.HorizontalButtonsCount,
                VerticalButtonsCount = launchPad.VerticalButtonsCount,
                RGBLightsEnabled = launchPad.RGBLightsEnabled,
                UsbTypeCPortEnabled = launchPad.UsbTypeCPortEnabled,
                JackInputEnabled = launchPad.JackInputEnabled,
                MicroSDCardSlotEnabled = launchPad.MicroSDCardSlotEnabled,
                USBStorageEnabled = launchPad.USBStorageEnabled
            };
        }
    }