#include <string>
#include <iostream>
#include <algorithm>
#include "classes.hpp"

void Coding(int value, Signal *signal1)
{
  signal1->signalValue = value;
  std::cout << signal1->Request(signal1->signalValue);
}

int main() 
{
  Signal *signal = new Signal;
  int newSignal;
  std::cout<< "Indique los bits que deben ser transformados: "<< std::endl;
  std::cin >> newSignal;
  signal->signalValue = newSignal;
  Coding(newSignal, signal);
  std::cout <<std::endl;
  Adaptee *adaptee = new Adaptee;
  std::cout << "Se esta transformando la siguiente senal: ";
  std::cout << adaptee->SpecificRequest(newSignal);
  std::cout <<std::endl;
  std::cout << "La senal transformada es la siguiente: ";
  Adapter *adapter = new Adapter(adaptee);
  adapter->signalValue = newSignal;
  Coding(newSignal, adapter);
  std::cout <<std::endl<<std::endl;;
  delete signal;
  delete adaptee;
  delete adapter;
  return 0;
}