#include <string>
#include <iostream>
#include <algorithm>


class Signal 
{
public:
    int signalValue;
    virtual ~Signal() = default;
    virtual std::string Request(int) const 
    {
        return "Iniciando tranformacion... ";
    }
};

class Adaptee 
{
public:
    int SpecificRequest(int digitalSignal) const 
    {
        return digitalSignal;
    }
};

class Adapter : public Signal 
{
private:
    Adaptee *adaptee_;

public:
    int signalValue;
    Adapter(Adaptee *adaptee) : adaptee_(adaptee) {}
    std::string Request(int digitalSignal) const override
    {
        long int hex[1000];
        int i = 1;
        int j = 0;
        int rem; 
        int dec = 0;
        char test;
        while (digitalSignal > 0) 
        {
            rem = digitalSignal % 2;
            dec = dec + rem * i;
            i = i * 2;
            digitalSignal = digitalSignal / 10;
        }
            i = 0;
        while (dec != 0) 
        {
            hex[i] = dec % 16;
            dec = dec / 16;
            i++;
        }
        for (j = i - 1; j >= 0; j--)
        {
            if (hex[j] > 9)
            {
                std::cout<<(char)(hex[j] + 55);
            }
            else
            {
                std::cout<<hex[j];
            }
        }
        std::cout<<std::endl;
        return "Proceso finalizado.";
    }
};